#  IOS TEST

## Given:

### A starter project with 2 viewcontrollers :

ViewController 
DetailViewController

Need to add whatever is missing to result in:

## Requirements:

ViewController.

1. Is presented on launch.

2. Has a single TextField, round rect border style, centered vertically with 20pt leading and trailing from edges.

3. When return is pressed on keyboard, if text has been entered into the TextField, it is then saved into the keychain with an arbitrary key of your choosing, then 

4. DetailViewController presented modally.

DetailViewController

1. Is presented from ViewController

2. Has a UILabel, centered vertically with 20pt leading and trailing from edges.

3. When controller is presented, the value previously saved in the keychain is retrieved from the keychain, and used to populate the label.

4. User can dismiss DetailViewController and return to ViewController easily.


## Guidelines:
* Dont use storyboards
* UI to be created programatically including layout constraints
* It is okay to use a thirdparty wrapper around the keychain (eg cocoapod SwiftKeychainWrapper). 

## Expectations
* Project grouping and structuring
* Protocol orientated programming
* Demonstrate seperation of concerns
* The use of dependency injection

## Bonus points:
some unit tests

